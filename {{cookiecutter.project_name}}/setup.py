from setuptools import setup, find_packages

NAME = "{{cookiecutter.project_name}}"
VERSION = "0.0.1"

setup(name=NAME,
      version=VERSION,
      description="{{cookiecutter.project_description}}",
      author="{{cookiecutter.author_full_name}}",
      author_email="{{cookiecutter.author_email}}",
      url="{{cookiecutter.project_url}}",
      packages=find_packages())

