import json

from django.test import TestCase
from django.urls import reverse

class TestStatusInfoView(TestCase):
    def test_status_info_view(self):
        raw_response = self.client.get(reverse("StatusInfoView"))
        self.assertEquals(raw_response.status_code, 200)
        response = json.loads(raw_response.content)
        self.assertEquals(response.get("status"), "ok")

