import datetime

from django.conf import settings
from django.http import JsonResponse
from django.shortcuts import render
from django.utils.timezone import utc
from django.views.generic import TemplateView

# Create your views here.

class StatusInfoView(TemplateView):
    def get(self, request):
        response = {
            "time": datetime.datetime.utcnow().replace(tzinfo=utc),
            "environment": settings.ENVIRONMENT,
            "status": "ok",
        }
        return JsonResponse(response)

