from django.contrib import admin
from django.urls import path

from apps.core import views as core_views

urlpatterns = [
    path('status', core_views.StatusInfoView.as_view(), name="StatusInfoView"),
    path('admin/', admin.site.urls),
]
