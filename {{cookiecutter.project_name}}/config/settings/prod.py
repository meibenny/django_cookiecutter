from .base import *

DEBUG = False
DEPLOY_PORT = "{{cookiecutter.deploy_port}}"
ENVIRONMENT = "production"
