import os

from django.conf import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings.base')

bind = "127.0.0.1:" + settings.DEPLOY_PORT
workers = 2
max_requests = 1000
max_requests_jitter = 100
